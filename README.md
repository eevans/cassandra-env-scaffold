# cassandra-env-scaffold

Scaffolding for projects that need a Cassandra development and/or testing environment.


## Requirements

- Docker & Docker Compose
- Make

On Debian-based systems, `apt install build-essential docker-compose` is probably sufficient.


## Usage

### Running
    
    $ make up
    docker-compose up -d
    Creating network "env-scaffold_cassandra" with the default driver
    Creating cassandra0 ... done
    Creating cassandra1 ... done
    Creating cassandra2 ... done
    $
    
Starts a three node Cassandra cluster in the background, binding to 0.0.0.0:904[2-4].


### Connecting with cqlsh

Running `cqlsh` from an (ephemeral) container:

    $ make cqlsh


### Creating schema & loading test data

    $ make bootstrap

The `bootstrap` target is a convenience that reads files with a `.cql` extension (in sort lexical order)
from `bootstrap.d`, and runs them with `cqlsh -f`.


### Shutting down

To terminate the cluser (and remove all associated state and containers):

    $ make down clean
    docker-compose down --remove-orphans
    Stopping cassandra2 ... done
    Stopping cassandra1 ... done
    Stopping cassandra0 ... done
    Removing cassandra2 ... done
    Removing cassandra1 ... done
    Removing cassandra0 ... done
    Removing network env-scaffold_cassandra
    $
	
