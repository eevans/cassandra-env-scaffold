
# This needs to match a service name docker-compose.yml
NODE ?= cassandra0


all: ps

bootstrap:
	for f in `find bootstrap.d/ -name '*.cql' -printf '%f\n'`; do \
	    docker exec -it $(NODE) cqlsh $(NODE) -f /env/$$f; \
	done

clean: down
	@sudo rm -rf data/cassandra0/{commitlog,data,hints,saved_caches}
	@sudo rm -rf data/cassandra1/{commitlog,data,hints,saved_caches}
	@sudo rm -rf data/cassandra2/{commitlog,data,hints,saved_caches}

cqlsh:
	@docker exec -it $(NODE) cqlsh $(NODE)

down:
	@docker-compose down --remove-orphans

up: _up version
_up:
	@docker-compose up -d

ps:
	@docker-compose ps

restart:
	@docker-compose restart

status:
	@docker exec -it $(NODE) nodetool status

version:
	@docker exec -it $(NODE) nodetool version

.PHONY: bootstrap cqlsh down ps restart status up version
